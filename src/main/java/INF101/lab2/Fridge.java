package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxSize;
    ArrayList<FridgeItem> fridgeArray;

    public Fridge() {
        fridgeArray = new ArrayList<FridgeItem>();
        maxSize = 20;
    }

    @Override
    public int nItemsInFridge() {
        return fridgeArray.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeArray.size() < maxSize) {
            fridgeArray.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        try {
            fridgeArray.remove(fridgeArray.indexOf(item));
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeArray.removeAll(fridgeArray);
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : fridgeArray) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        fridgeArray.removeAll(expiredFood);
        return expiredFood;
    }
    
    
}
